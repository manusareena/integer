
APP = Integer
CC = g++
DEBUG =
CPP14FLAG = -std=c++14
CPPFLAGS = -c -Wall
LDFLAGS =
SOURCES = Integer.cpp Main.cpp
OBJECTS = $(SOURCES:%.cpp=%.o)

.PHONY: all
all: $(SOURCES) $(APP)

$(APP):	$(OBJECTS)
	$(CC) $(DEBUG) $(CPP14FLAG) $(LDFLAGS) $(OBJECTS) -o $@

%.o:	%.cpp
	$(CC) $(DEBUG) $(CPP14FLAG) $(CPPFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -f $(OBJECTS) $(APP)

