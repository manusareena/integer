#include "Integer.hpp"

Integer::Integer() : Sign{Positive}, Digits{'0'}
{ }

Integer::Integer(int Int)
{
    Sign = Positive;
    if (Int == 0)
    {
        Digits = {'0'};
    }
    else if (Int < 0)
    {
        Int *= -1;
        Sign = Negative;
    }

    while (Int > 0)
    {
        char Digit = static_cast<char>((Int % 10) + 48);
        Digits.push_back(Digit);
        Int /= 10;
    }
}

Integer::Integer(std::string const& StrInt)
{
    Sign = Positive;
    if (StrInt.size() == 0)
    {
        Digits = {'0'};
    }
    else
    {
        int LastDigitPos = 0;

        if (StrInt.front() == '-')
        {
            Sign = Negative;
            LastDigitPos = 1;
        }
        else if (StrInt.front() == '+')
        {
            LastDigitPos = 1;
        }

        for (int i = StrInt.size() - 1; i >= LastDigitPos; --i)
        {
            // Non digit characters will result in 0
            if (StrInt[i] < 48 || StrInt[i] > 57)
            {
                Sign = Positive;
                Digits = {'0'};
                break;
            }
            Digits.push_back(StrInt[i]);
        }

        RemoveLeadingZeroes();
    }
}

Integer::Integer(Integer const& Num)
{
    Sign = Num.IsPositive() ? Positive : Negative;
    Digits = std::vector<char>(Num.Size());
    for (int i = 0; i < Num.Size(); ++i)
    {
        Digits[i] = static_cast<char>(Num[i] + 48);
    }
}

Integer::Integer(Integer&& Num) : Sign{Num.Sign},
                                  Digits{std::move(Num.Digits)}
{ }

Integer& Integer::operator =(Integer const& Num)
{
    Sign = Num.IsPositive() ? Positive : Negative;
    Digits = std::vector<char>(Num.Size());
    for (int i = 0; i < Num.Size(); ++i)
    {
        Digits[i] = static_cast<char>(Num[i] + 48);;
    }

    return *this;
}

Integer& Integer::operator =(Integer&& Num)
{
    Digits = std::move(Num.Digits);
    Sign = Num.Sign;
    return *this;
}

bool Integer::Equal(Integer const& Num) const
{
    if (Size() != Num.Size() || GetSign() != Num.GetSign())
    {
        return false;
    }

    for (int i = 0; i < Size(); ++i)
    {
        if ((*this)[i] != Num[i])
        {
            return false;
        }
    }

    return true;
}

bool Integer::LesserThan(Integer const& Num) const
{
    // LHS is negative and RHS is positive or RHS is negative with less digits
    if (IsNegative() && Num.IsPositive())
    {
        return true;
    }
    // LHS is positive and RHS is negative or RHS is positive with more digits
    if (IsPositive() && Num.IsNegative())
    {
        return false;
    }

    if (Size() != Num.Size())
    {
        if (IsPositive())
        {
            return Size() < Num.Size();
        }
        else
        {
            return Size() > Num.Size();
        }
    }

    // RHS and LHS have the same sign and size
    for (int i = Size() - 1; i >= 0; --i)
    {
        if ((*this)[i] < Num[i])
        {
            return (IsPositive()) ? true : false;
        }
        else if ((*this)[i] > Num[i])
        {
            return (IsPositive()) ? false : true;
        }
    }

    // The numbers are equal
    return false;
}

bool Integer::GreaterThan(Integer const& Num) const
{
    return !LesserThan(Num) && !Equal(Num);
}

Integer const& Integer::Add(Integer const& Num)
{
    if ((IsNegative() && Num.IsPositive()) ||
        (IsPositive() && Num.IsNegative()))
    {
        if (*this == -Num)
        {
            *this = 0;
        }
        else
        {
            Integer Num1{*this};
            Integer Num2{Num};

            Num1.SetSign(Positive);
            Num2.SetSign(Positive);
            SignType NewSign = (Num1 > Num2) ? Sign : Num.GetSign();
            *this = Num1 - Num2;
            Sign = NewSign;
        }
        return *this;
    }

    int Length = Size() < Num.Size() ? Num.Size() : Size();

    int Result = 0;
    int Carry = 0;
    for (int i = 0; i < Length; ++i)
    {
        int DigitSum = (*this)[i] + Num[i] + Carry;
        Result = DigitSum % 10;
        char ResultDigit = static_cast<char>(Result + 48);
        if (i < Size())
        {
            Digits[i] = ResultDigit;
        }
        else
        {
            Digits.push_back(ResultDigit);
        }
        Carry = DigitSum / 10;
    }

    if (Carry)
    {
        Digits.push_back(static_cast<char>(49));
    }
    
    return *this;
}

Integer const& Integer::Subtract(Integer const& Num)
{
    // Subtraction is carried out by subtracting absolute value of smaller
    // number from the absolute value of the bigger number, and prefixing the
    // correct sign.
    if ((IsNegative() && Num.IsPositive()) ||
        (IsPositive() && Num.IsNegative()))
    {
        Integer Result{-Num};
        *this += Result;
        RemoveLeadingZeroes();
        return *this;
    }

    if ((IsPositive() && (*this < Num)) ||
        (IsNegative() && (*this > Num)))
    {
        *this = Num - *this;
        Sign = IsPositive() ? Negative : Positive;
        RemoveLeadingZeroes();
        return *this;
    }

    int Length = Size() < Num.Size() ? Num.Size() : Size();
    int Borrow = 0;

    for (int i = 0; i < Length; ++i)
    {
        int Minuend = (*this)[i] - Borrow;
        int Subtrahend = Num[i];

        if (Minuend < Subtrahend)
        {
            Minuend += 10;
            Borrow = 1;
        }
        else
        {
            Borrow = 0;
        }

        int Result = Minuend - Subtrahend;
        Digits[i] = static_cast<char>(Result + 48);
    }

    RemoveLeadingZeroes();
    return *this;
}

Integer const& Integer::Multiply(Integer const& Num)
{
    if (Num == 0)
    {
        *this = 0;
        Sign = Positive;
    }

    if (*this == 0 || Num == 1)
    {
        return *this;
    }

    Integer This{*this};
    Sign = (GetSign() != Num.GetSign()) ? Negative : Positive;
    int Length = (Size() > Num.Size()) ? Size() : Num.Size();
    int Carry = 0;
    
    for (int Pos = 0; Pos < Length; ++Pos)
    {
        int PosProduct = 0;
        for (int Index = 0; Index <= Pos; ++Index)
        {
            int Index1 = Index;
            int Index2 = Pos - Index;
            PosProduct += This[Index1] * Num[Index2];  
        }
        PosProduct += Carry;
        int Digit = PosProduct % 10;

        if (Pos < Size())
        {
            Digits[Pos] = static_cast<char>(Digit+ 48); 
        }
        else
        {
            Digits.push_back(static_cast<char>(Digit+ 48));
        }
        
        Carry = PosProduct / 10;
    }

    for (int Pos = 1; Pos < Length ; ++Pos)
    {
        int PosProduct = 0;
        for (int Index = Length - 1; Index >= Pos; --Index)
        {
            int Index1 = Index;
            int Index2 = Pos + Length - 1 - Index;
            PosProduct += This[Index1] * Num[Index2];  
        }
        PosProduct += Carry;
        int Digit = PosProduct % 10;

        Digits.push_back(static_cast<char>(Digit+ 48));
        Carry = PosProduct / 10;
    }

    while (Carry)
    {
        int Digit = Carry % 10;
        Digits.push_back(static_cast<char>(Digit+ 48));
        Carry /= 10;
    }

    RemoveLeadingZeroes();
    return *this;
}

Integer const& Integer::Divide(Integer const& Num)
{
    if (Abs() < Num.Abs())
    {
        *this = 0;
        Sign = Positive;
    }

    // If divisor is zero, we return the dividend
    if (*this == 0 || Num == 1 || Num == 0)
    {
        return *this;
    }
    
    Integer Quotient{0};
    Integer Carry{0};
    for (int Index = 1; Index <= Size(); ++Index)
    {
        Quotient *= 10;
        Carry *= 10;
        Integer Dividend = Integer{(*this)[Size() - Index]} + Carry;

        if (Dividend == 0 && Index == Size())
        {
            break;
        }
        while (Dividend < Num)
        {
            Quotient *= 10;
            Dividend *= 10;
            ++Index;
            Dividend += Integer{(*this)[Size() - Index]};
        }

        Integer Product{Num.Abs()};
        while (Dividend >= Product)
        {
            Product += Num.Abs();
            ++Quotient;
        }

        Carry = Num.Abs() - (Product - Dividend); 
    }

    SignType NewSign = GetSign() != Num.GetSign() ? Negative : Positive;
    *this = Quotient;
    Sign = NewSign;

    return *this;
}

Integer const& Integer::Power(Integer const& Num)
{
    if (Num < 0)
    {
        *this = 0;
    }
    else if (Num == 0)
    {
        *this = 1;
    }
    else if (Num > 1)
    {
        std::unordered_map<int, Integer> PowerMap;
        int Exp{1};
        PowerMap[Exp] = *this;

        while (Exp < Num)
        {
            Exp <<= 1;
            PowerMap[Exp] = PowerMap[(Exp >> 1)] * PowerMap[(Exp >> 1)];
        }

        Integer Result{1};
        int Counter{0};

        while (Counter < Num)
        {
            if (Counter + Exp <= Num)
            {
                Counter += Exp;
                Result *= PowerMap[Exp];
            }
            Exp >>= 1;
        }

        *this = Result;
    }
    return *this;
}

Integer Integer::Abs() const
{
    return (IsNegative()) ? -(*this) : *this;
}

Integer Integer::Mod(Integer const& Num) const
{
    Integer Dividend{*this};
    Integer Quotient = Dividend / Num;
    Integer Remainder = Dividend - (Quotient * Num);
    return Remainder;
}

std::ostream& Integer::Print(std::ostream& os) const
{
    if (IsNegative())
    {
        os << '-';
    }

    for (int i = Size() - 1; i >= 0; --i)
    {
        os << Digits[i];
    }

    return os;
}

void Integer::RemoveLeadingZeroes()
{
    // Remove all the leading zeroes
    auto FirstDigitIt = Digits.end() - 1;
    for (; FirstDigitIt != Digits.begin() && *FirstDigitIt == '0';
           --FirstDigitIt);
    Digits.erase(FirstDigitIt + 1, Digits.end());
}

Integer const& Integer::operator += (Integer const& Num)
{
    Integer Result{Num};
    Add(Result);
    return *this;
}

Integer const& Integer::operator -= (Integer const& Num)
{
    Integer Result{Num};
    Subtract(Result);
    return *this;
}

Integer const& Integer::operator *=(Integer const& Num)
{
    Integer Result{Num};
    Multiply(Result);
    return *this;
}

Integer const& Integer::operator /=(Integer const& Num)
{
    Integer Result{Num};
    Divide(Result);
    return *this;
}

Integer const& Integer::operator ^=(Integer const& Num)
{
    Power(Num);
    return *this;
}

Integer operator -(Integer const& Num)
{
    Integer Result{Num};
    Result.SetSign(Num.IsPositive() ? Integer::Negative : Integer::Positive);
    return Result;
}

Integer const& Integer::operator ++()
{
    Add(Integer{1});
    return *this;
}

Integer Integer::operator ++(int)
{
    Integer Result{*this};
    operator++();
    return Result;
}

Integer const& Integer::operator --()
{
    *this -= Integer{1};
    return *this;
}

Integer Integer::operator --(int)
{
    Integer Result{*this};
    operator--();
    return Result;
}

Integer operator +(Integer const& Num1, Integer const& Num2)
{
    Integer Result{Num1};
    Result += Num2;
    return Result;
}

Integer operator -(Integer const& Num1, Integer const& Num2)
{
    Integer Result{Num1};
    Result -= Num2;
    return Result;
}

Integer operator *(Integer const& Num1, Integer const& Num2)
{
    Integer Result{Num1};
    Result *= Num2;
    return Result;
}

Integer operator /(Integer const& Num1, Integer const& Num2)
{
    Integer Result{Num1};
    Result /= Num2;
    return Result;
}

Integer operator ^(Integer const& Num1, Integer const& Num2)
{
    Integer Result{Num1};
    Result ^= Num2;
    return Result;
}

Integer operator %(Integer const& Num1, Integer const& Num2)
{
    Integer Result = Num1.Mod(Num2);
    return Result;
}

std::istream& operator>>(std::istream& is, Integer& Num)
{
    std::string StrInt;
    is >> StrInt;
    Num = Integer(StrInt);
    return is;
}

std::ostream& operator<<(std::ostream& os, Integer const& Num)
{
    Num.Print(os);
    return os;
}

bool operator ==(Integer const& Num1, Integer const& Num2)
{
    return Num1.Equal(Num2);
}

bool operator !=(Integer const& Num1, Integer const& Num2)
{
    return !(Num1 == Num2);
}

bool operator <(Integer const& Num1, Integer const& Num2)
{
    bool Result = Num1.LesserThan(Num2);
    return Result;
}

bool operator >(Integer const& Num1, Integer const& Num2)
{
    bool Result = Num1.GreaterThan(Num2);
    return Result;
}

bool operator <=(Integer const& Num1, Integer const& Num2)
{
    return !Num1.GreaterThan(Num2);
}

bool operator >=(Integer const& Num1, Integer const& Num2)
{
    return !Num1.LesserThan(Num2);
}
