#ifndef __Integer_HPP__
#define __Integer_HPP__

#include <type_traits>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
#include <utility>

class Integer
{
public:
    enum SignType {Negative, Positive};

    Integer();
    Integer(int Int);
    Integer(std::string const& StrInt);
    Integer(Integer const& Num);
    Integer(Integer&& Num);

    inline int Size() const { return Digits.size(); }
    inline SignType GetSign() const { return Sign; }
    inline void SetSign(SignType NewSign) { Sign = NewSign; }
    inline bool IsPositive() const { return Sign == Positive; }
    inline bool IsNegative() const { return Sign == Negative; }
    Integer Abs() const;
    
    bool Equal(Integer const& Num) const;
    bool LesserThan(Integer const& Num) const;
    bool GreaterThan(Integer const& Num) const;

    Integer const& Add(Integer const& Num);
    Integer const& Subtract(Integer const& Num);
    Integer const& Multiply(Integer const& Num);
    Integer const& Divide(Integer const& Num);
    Integer const& Power(Integer const& Num);
    Integer Mod(Integer const& Num) const;
    std::ostream& Print(std::ostream& os) const;

    Integer& operator =(Integer const& Num);
    Integer& operator =(Integer&& Num);
    Integer const& operator +=(Integer const& Num);
    Integer const& operator -=(Integer const& Num);
    Integer const& operator *=(Integer const& Num);
    Integer const& operator /=(Integer const& Num);
    Integer const& operator ^=(Integer const& Num);

    Integer const& operator ++();
    Integer operator ++(int);
    Integer const& operator --();
    Integer operator --(int);

private:
    SignType Sign;
    std::vector<char> Digits;

    inline int operator[](int const Index) const
    {
        if (Index < 0 || Index >= Size())
        {
            return 0;
        }
        return static_cast<int>(Digits[Index] - 48) ;
    }

protected:
    void RemoveLeadingZeroes();
};

Integer operator -(Integer const& Num);
Integer operator %(Integer const& Num1, Integer const& Num2);
std::istream& operator >>(std::istream& is, Integer& Num);
std::ostream& operator <<(std::ostream& os, Integer const& Num);
bool operator ==(Integer const& Num1, Integer const& Num2);
bool operator !=(Integer const& Num1, Integer const& Num2);
bool operator <(Integer const& Num1, Integer const& Num2);
bool operator >(Integer const& Num1, Integer const& Num2);
bool operator <=(Integer const& Num1, Integer const& Num2);
bool operator >=(Integer const& Num1, Integer const& Num2);
Integer operator +(Integer const& Num1, Integer const& Num2);
Integer operator -(Integer const& Num1, Integer const& Num2);
Integer operator *(Integer const& Num1, Integer const& Num2);
Integer operator /(Integer const& Num1, Integer const& Num2);
Integer operator ^(Integer const& Num1, Integer const& Num2);

#endif