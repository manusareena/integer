#include <iostream>
#include <random>
#include "Integer.hpp"

int main()
{
    // std::random_device RD;
    // std::mt19937 Gen(RD());
    // std::uniform_int_distribution<int> Dis1(-76922, 769232);
    // std::uniform_int_distribution<int> Dis2(-45232, 986704);

    while(true)
    {
        // int IntA = Dis1(Gen);
        // int IntB = Dis2(Gen);
        // Integer A{IntA};
        // Integer B{IntB};

        Integer A;
        Integer B;
        char Operator;
        std::cout << ">> ";
        // std::cin >> Operator;
        std::cin >> A >> Operator >> B;

        Integer Result;
        // long long IntResult;
        bool    Compare;
        // bool    IntCompare;
        switch(Operator)
        {
        case '+':
            Result = A + B;
            //IntResult = IntA + IntB;
            break;

        case '-':
            Result = A - B;
            //IntResult = IntA - IntB;
            break;

        case '*':
            Result = A * B;
            // IntResult = static_cast<long long>(IntA) * IntB;
            break;

        case '/':
            Result = A / B;
            // IntResult = IntA / IntB;
            break;

        case '^':
            Result = A ^ B;
            // IntResult = IntA ^ IntB;
            break;

        case '%':
            Result = A % B;
            break;

        case '>':
            Compare = A > B;
            //IntCompare = IntA > IntB;
            break;

        case '<':
            Compare = A < B;
            //IntCompare = IntA < IntB;
            break;
        
        case '=':
            Compare = A == B;
            //IntCompare = IntA == IntB;
            break;

        default:
            continue;
        }

        std::cout << A << " " << Operator << " " << B << " = ";

        if (Operator == '+' || Operator == '-' ||
            Operator == '*' || Operator == '/' ||
            Operator == '^' || Operator == '%')
        {
            std::cout << Result << std::endl;
        }
        else
        {
            std::cout << ((Compare) ? "True" : "False") << std::endl;
        }

        // std::cout << IntA << " " << Operator << " " << IntB << " = ";

        // if (Operator == '+' || Operator == '-' ||
        //     Operator == '*' || Operator == '/' ||
        //     Operator == '^' || Operator == '%')
        // {
        //     std::cout << IntResult << std::endl;
        // }
        // else
        // {
        //     std::cout << ((IntCompare) ? "True" : "False") << std::endl;
        // }
    }

    return 0;
}
