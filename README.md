# README #

### What is this repository for? ###

* Quick summary:
An integer class in C++ to handle large integers that cannot be handled by built-in
data types.
* Version
1.0

### How do I get set up? ###

* Summary of set up
Include the header file in your source code and link the application object
to the library object during linking.
The Main.cpp file is provided for testing and requires ^C to exit out of the main loop.
* Dependencies
Linking to this library will require C++11/14 flag specification.

### Who do I talk to? ###

* Repo owner
manu.sareena@gmail.com